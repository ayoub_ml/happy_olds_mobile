/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.util.ArrayList;
import entity.User;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 *
 * @author autonome
 */
public class UserService {
    ArrayList<User> listUser ;
    
    public ArrayList<User> getAllUser() {
        listUser = new ArrayList<>()  ;
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/3A11/happy_olds/web/app_dev.php/mobile/Agee/afficher");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                listUser = subGetAllUser(new String(con.getResponseData()));

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listUser;
    }
    
    
    public ArrayList<User> subGetAllUser(String json) {

        ArrayList<User> u = new ArrayList<>();

        try {
            JSONParser j = new JSONParser();

            Map<String, Object> p = j.parseJSON(new CharArrayReader(json.toCharArray()));
            System.out.println("__________p to string"+p.toString());
            List<Map<String, Object>> list = (List<Map<String, Object>>) p.get("root");
            for (Map<String, Object> obj : list) {
                
                String nom = obj.get("nom").toString();
                String prenom = obj.get("prenom").toString();

                float id = Float.parseFloat(obj.get("id").toString());
                float cin = Float.parseFloat(obj.get("cin").toString());
         
                User agee = new User ((int) id , (int) cin , nom , prenom);
                u.add(agee);
                

            }

        } catch (Exception ex) {
        }
        

        return u;

    }
    
}

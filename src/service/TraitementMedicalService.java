/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.sendgrid.SendGrid;
import com.codename1.ui.events.ActionListener;
import entity.TraitementMedical;
import entity.User;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 *
 * @author autonome
 */
public class TraitementMedicalService {
    
    
    
    
    public void ajoutTraitement(TraitementMedical traitement) {
        //  http://localhost/3A11/happy_olds/web/app_dev.php/mobile/traitement/
        //  ajouter?id=3&nomMedicament=medicmnt%20Abc&traitementDesc=3%20Comprimee&duree=40&dejeuner=avant&petitDejeuner=non&diner=non
        ConnectionRequest con = new ConnectionRequest();
        String Url = "http://localhost/3A11/happy_olds/web/app_dev.php/mobile/traitement/"
                + "ajouter?id="     +traitement.getAgee().getId()
                +"&nomMedicament="  +traitement.getNomMedicament()
                +"&traitementDesc=" +traitement.getTraitementDesc()
                +"&duree="          +traitement.getDureeEnJourDeTraitement()
                +"&dejeuner="       +traitement.getDejeuner()
                +"&petitDejeuner="  +traitement.getPetitDejeuner()
                +"&diner="          +traitement.getDiner();
        con.setUrl(Url);
        
        
        
        
        String jourS = " jours";
        if(traitement.getDureeEnJourDeTraitement()==0){
            jourS = "jour";
        }
        String AvantApres = "";
        if(traitement.getPetitDejeuner().equals("avant")){
            AvantApres += " Vous devez prondre ce médicament avant le Petit déjeuner";
        }else if(traitement.getPetitDejeuner().equals("apres")){
            AvantApres += " Vous devez prondre ce médicament après le Petit déjeuner";
        }
        else {
            if (traitement.getDejeuner().equals("avant")){
                AvantApres += " Vous devez prondre ce médicament avant le Déjeuner";
            }else if(traitement.getDejeuner().equals("apres")){
            AvantApres += " Vous devez prondre ce médicament après le Déjeuner";
            }else {
                if (traitement.getDiner().equals("avant")){
                AvantApres += " Vous devez prondre ce médicament avant le Dîner";
                }else if(traitement.getDiner().equals("apres")){
                AvantApres += " Vous devez prondre ce médicament après le Dîner";
                }
            }
        }
        
        
        if(traitement.getDejeuner().equals("avant") && traitement.getDiner().equals("non") && ! traitement.getPetitDejeuner().equals("non")){
            AvantApres += ", et avant le Déjeuner";
        }else if(traitement.getDejeuner().equals("apres") && traitement.getDiner().equals("non") && ! traitement.getPetitDejeuner().equals("non")){
            AvantApres += ", et après le Déjeuner";
        }
       
        
        if(traitement.getDejeuner().equals("avant") && ! traitement.getPetitDejeuner().equals("non") && ! traitement.getDiner().equals("non")){
            AvantApres += ", avant le Déjeuner";
        }else if(traitement.getDejeuner().equals("apres")&& ! traitement.getPetitDejeuner().equals("non")&& ! traitement.getDiner().equals("non")){
            AvantApres += ", après le Déjeuner";
        }
        
        
        if(traitement.getDiner().equals("avant")&& ! traitement.getPetitDejeuner().equals("non")&& ! traitement.getDejeuner().equals("non")){
            AvantApres += ", et avant le Dîner";
        }else if(traitement.getDiner().equals("apres")&& ! traitement.getPetitDejeuner().equals("non")&& ! traitement.getDejeuner().equals("non")){
            AvantApres += ", et après le Dîner";
        }
        
        
        
        
        
        SendGrid s = SendGrid.create("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");
//        SendGrid s = SendGrid.create("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");
        s.sendSync("ayoub.mlaouah@esprit.tn"
                , "ayoub.mlaouah@esprit.tn"
                , "Happy_Olds"
                , "Bonjour Monsieur "
                        +traitement.getAgee().getPrenom()
                        +". Vous avez un Traitement médicamenteux à suivre."
                        +" Votre Docteur du centre Happy_Old vous a ordonnoncé le médicament  "
                        +traitement.getNomMedicament()+", dont vous devez le prendre pendant "
                        +traitement.getDureeEnJourDeTraitement()+jourS
                        +". A noter: "+traitement.getTraitementDesc()+". "
                        +AvantApres
                        +". Happy old vous souhaite une bonne santée..."
                        );


            con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            //System.out.println(str);

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    
    ArrayList<TraitementMedical> listTraitement ;
    
    public ArrayList<TraitementMedical> getAllTraitementOfUser(User agee) {
//                    System.out.println("___________________frooom getAllTraitementOfUser____________________");
        listTraitement = new ArrayList<>()  ;
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/3A11/happy_olds/web/app_dev.php/mobile/traitement/recherche/idAgee/"+agee.getId());
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
//                System.out.println("___________________avant apl subGetAllTraitementOfUser ____________________");
                listTraitement = subGetAllTraitementOfUser(new String(con.getResponseData()), agee);

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listTraitement;
    }
    
    
    public ArrayList<TraitementMedical> subGetAllTraitementOfUser(String json , User agee) {
//                System.out.println("___________________frooom subGetAllTraitementOfUser ____________________");

        ArrayList<TraitementMedical> u = new ArrayList<>();

        try {
            JSONParser j = new JSONParser();
            Map<String, Object> p = j.parseJSON(new CharArrayReader(json.toCharArray()));
//            System.out.println("_____________ p root to string"+p.get("root").toString());
//                System.out.println("_____________ Map to string"+p.toString());
                
//                for (Map.Entry<String, Object> entry : p.entrySet()) {
//      System.out.println("Key : " + entry.getKey() + " value : " + entry.getValue());
//    }
            List<Map<String, Object>> list = (List<Map<String, Object>>) p.get("root");
                System.out.println("_____________list to string"+list.toString());
            for (Map<String, Object> obj : list) {
//                    System.out.println("_________________  from for-each   ____________________");
                String nomMedicament = obj.get("nomMedicament").toString();
//                    System.out.println("_______________________________________"+nomMedicament);
                String traitementDesc = obj.get("traitementDesc").toString();
//                    System.out.println("_______________________________________"+traitementDesc);
                String dejeuner = obj.get("dejeuner").toString();
//                    System.out.println("_______________________________________"+dejeuner);
                String petitDejeuner = obj.get("petitDejeuner").toString();
//                    System.out.println("_______________________________________"+petitDejeuner);
                String diner = obj.get("diner").toString();
//                    System.out.println("_______000000000000000000000000000000000000000000000_________"+diner);

                float id = Float.parseFloat(obj.get("id").toString());
//                float idAgee = Float.parseFloat(obj.get("idAgee").toString());
                float dureeEnJourDeTraitement = Float.parseFloat(obj.get("dureeEnJourDeTraitement").toString());

                TraitementMedical traitement = new TraitementMedical ((int) id , (int) dureeEnJourDeTraitement , agee.getId() , nomMedicament , traitementDesc , dejeuner , petitDejeuner , diner  );
//                System.out.println(traitement.toString());
                u.add(traitement);
//                System.out.println("u traitmnt to string_______"+u.toString());

            }

        } catch (Exception ex) {
//            System.out.println("______________from ex______________");
            ex.printStackTrace();
        }
        

        return u;

    }
    
    
    public void modifTraitement(TraitementMedical traitement , User agee) {
        //http://localhost/3A11/happy_olds/web/app_dev.php/mobile/traitement/
        //update?idTraitement=1&idAgee=1&nomMedicament=nouveau
        //&traitementDesc=nouv%20desc&dureeEnJourDuTraitement=6
        ConnectionRequest con = new ConnectionRequest();
        String Url = "http://localhost/3A11/happy_olds/web/app_dev.php/mobile/traitement/"
                + "update?idTraitement="     +traitement.getId()
                + "&idAgee="     +agee.getId()
                +"&nomMedicament="  +traitement.getNomMedicament()
                +"&traitementDesc=" +traitement.getTraitementDesc()
                +"&duree="          +traitement.getDureeEnJourDeTraitement()
                +"&dejeuner="       +traitement.getDejeuner()
                +"&petitDejeuner="  +traitement.getPetitDejeuner()
                +"&diner="          +traitement.getDiner();
        con.setUrl(Url);

            con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            //System.out.println(str);

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    
    
    public void supprimer1(int id ) {
        ConnectionRequest con = new ConnectionRequest();
            String Url = ("http://localhost/3A11/happy_olds/web/app_dev.php/mobile/supprimerTraitement?idTraitement="+id);
            con.setUrl(Url);
            con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }

    public ArrayList<TraitementMedical> getAllTraitement() {
        listTraitement = new ArrayList<>()  ;
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/3A11/happy_olds/web/app_dev.php/mobile/traitement/afficher");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
//                System.out.println("___________________avant apl subGetAllTraitementOfUser ____________________");
                listTraitement = subGetAllTraitement(new String(con.getResponseData()));

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listTraitement;
    }
    
    
    public ArrayList<TraitementMedical> subGetAllTraitement(String json ) {
//                System.out.println("___________________frooom subGetAllTraitementOfUser ____________________");

        ArrayList<TraitementMedical> u = new ArrayList<>();

        try {
            JSONParser j = new JSONParser();
            Map<String, Object> p = j.parseJSON(new CharArrayReader(json.toCharArray()));
//            System.out.println("_____________ p root to string"+p.get("root").toString());
//                System.out.println("_____________ Map to string"+p.toString());
                
//                for (Map.Entry<String, Object> entry : p.entrySet()) {
//      System.out.println("Key : " + entry.getKey() + " value : " + entry.getValue());
//    }
            List<Map<String, Object>> list = (List<Map<String, Object>>) p.get("root");
                System.out.println("_____________list to string"+list.toString());
            for (Map<String, Object> obj : list) {
//                    System.out.println("_________________  from for-each   ____________________");
                String nomMedicament = obj.get("nomMedicament").toString();
//                    System.out.println("_______________________________________"+nomMedicament);
                String traitementDesc = obj.get("traitementDesc").toString();
//                    System.out.println("_______________________________________"+traitementDesc);
                String dejeuner = obj.get("dejeuner").toString();
//                    System.out.println("_______________________________________"+dejeuner);
                String petitDejeuner = obj.get("petitDejeuner").toString();
//                    System.out.println("_______________________________________"+petitDejeuner);
                String diner = obj.get("diner").toString();
//                    System.out.println("_______000000000000000000000000000000000000000000000_________"+diner);

                float id = Float.parseFloat(obj.get("id").toString());
//                float idAgee = Float.parseFloat(obj.get("idAgee").toString());
                float dureeEnJourDeTraitement = Float.parseFloat(obj.get("dureeEnJourDeTraitement").toString());

                TraitementMedical traitement = new TraitementMedical ((int) id , (int) dureeEnJourDeTraitement  , nomMedicament , traitementDesc , dejeuner , petitDejeuner , diner  );
//                System.out.println(traitement.toString());
                u.add(traitement);
//                System.out.println("u traitmnt to string_______"+u.toString());

            }

        } catch (Exception ex) {
//            System.out.println("______________from ex______________");
            ex.printStackTrace();
        }
        

        return u;

    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codename1.uikit.materialscreens;

import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.util.Resources;
import entity.TraitementMedical;
import entity.User;

/**
 *
 * @author autonome
 */
class ActionListenerDetailsMedicament {
    boolean alternerDetail = true ;
    Button btnModif;
    
    public ActionListenerDetailsMedicament(DetailTraitement dt,Button btn ,SpanLabel sl , TraitementMedical t , User agee , Resources res ) {
        btn.addActionListener((ActionListener) new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (alternerDetail == true){
                    sl.setText("durée du Traitement "+t.getDureeEnJourDeTraitement()+" jours");
                    alternerDetail = !alternerDetail;
                }else{
                    sl.setHidden(true);
                    sl.setText("");
                    alternerDetail = !alternerDetail;
                }
                
//            sl.setHidden(false);
//            sl.setText("aaa");
//            sl.setText("aaa");
//            dt.add(sl);
//            System.out.println("--------------2020202020202020202020---------------------l.get(i)  "+t);
//            System.out.println("------------------------2020202020202020202020-----------------  i  "+i);
//            new DetailTraitement(res, agee ,  t).show();
            }
        });
        btnModif = new Button();
                btnModif.addActionListener(a -> {
                    
                    ModifierTraitement mt = new ModifierTraitement(res , agee , t );
                    mt.show();
                });
        sl.setLeadComponent(btnModif);
    }
    
}

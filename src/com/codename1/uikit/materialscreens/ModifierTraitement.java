/*
 * Copyright (c) 2016, Codename One
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

package com.codename1.uikit.materialscreens;

import com.codename1.components.FloatingActionButton;
import com.codename1.components.MultiButton;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import static com.codename1.ui.Component.BOTTOM;
import static com.codename1.ui.Component.CENTER;
import static com.codename1.ui.Component.LEFT;
import static com.codename1.ui.Component.RIGHT;
import com.codename1.ui.Container;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.list.GenericListCellRenderer;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import entity.TraitementMedical;
import entity.User;
import static java.lang.Integer.parseInt;
import java.util.Map;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;
import service.TraitementMedicalService;
import service.UserService;

/**
 * Represents a user profile in the app, the first form we open after the walkthru
 *
 * @author Shai Almog
 */
public class ModifierTraitement extends SideMenuBaseForm {
    Label petitDejeuner ;
    Label dejeuner ;
    Label diner ;
    
    Button supp ;
    
    ComboBox <String> combo ;    
    ComboBox <String> combo1 ;
    ComboBox <String> combo2 ;
    ComboBox <String> combo3 ;
    
    TextField txtf1  ;
    TextField txtf2  ;
    TextField txtf3  ;
    
    SpanLabel slDetail ;
    
    boolean firstshow = true ;
    
    static boolean alternerDetail = true ;
    

    
    ArrayList<TraitementMedical> listTraitementAgee ;
    
    Button [] btnTab ;
    SpanLabel [] sl ;
    
    int [] tabColeur = {0xd997f1,0x5ae29d,0x4dc2ff} ;
    
    
    
    
    SpanLabel sl2 ;
    
    
    
    
    
    

    public ModifierTraitement(Resources res , User agee , TraitementMedical t ) {
        super(BoxLayout.y());
        
//        this.getToolbar().addCommandToLeftBar("", res.getImage("low.png"), e->{
//            ProfileForm pf = new ProfileForm(res);
//            pf.show();
//        });

        String jourS = " jours";
        if(t.getDureeEnJourDeTraitement()==0){
            jourS = "jour";
        }
        String AvantApres = "";
        if(t.getPetitDejeuner().equals("avant")){
            AvantApres += " Il doit prondre ce médicament avant le Petit déjeuner";
        }else if(t.getPetitDejeuner().equals("apres")){
            AvantApres += " Il doit prondre ce médicament après le Petit déjeuner";
        }
        else {
            if (t.getDejeuner().equals("avant")){
                AvantApres += " Il doit prondre ce médicament avant le Déjeuner";
            }else if(t.getDejeuner().equals("apres")){
            AvantApres += " Il doit prondre ce médicament après le Déjeuner";
            }else {
                if (t.getDiner().equals("avant")){
                AvantApres += " Il doit prondre ce médicament avant le Dîner";
                }else if(t.getDiner().equals("apres")){
                AvantApres += " Il doit prondre ce médicament après le Dîner";
                }
            }
        }
        
        
        if(t.getDejeuner().equals("avant") && t.getDiner().equals("non") && ! t.getPetitDejeuner().equals("non")){
            AvantApres += ", et avant le Déjeuner";
        }else if(t.getDejeuner().equals("apres") && t.getDiner().equals("non") && ! t.getPetitDejeuner().equals("non")){
            AvantApres += ", et après le Déjeuner";
        }
        
        
        if(t.getDejeuner().equals("avant")&& ! t.getPetitDejeuner().equals("non")&& ! t.getDiner().equals("non") ){
            AvantApres += ", avant le Déjeuner";
        }else if(t.getDejeuner().equals("apres") && ! t.getPetitDejeuner().equals("non") &&  ! t.getDiner().equals("non") ){
            AvantApres += ", après le Déjeuner";
        }
        
        if(t.getDiner().equals("avant")&& ! t.getPetitDejeuner().equals("non")&& ! t.getDejeuner().equals("non") ){
            AvantApres += ", et avant le Dîner";
        }else if(t.getDiner().equals("apres")&& ! t.getPetitDejeuner().equals("non")&& ! t.getDejeuner().equals("non")){
            AvantApres += ", et après le Dîner";
        }

        getStyle().setBgColor(0xffffff);

        slDetail = new SpanLabel() ;
        
        sl2 = new SpanLabel() ;
//        sl2.setText("Nom du Medicament: "+t.getNomMedicament()+"  nomagee "+agee.getPrenom()+" id :"+agee.getId());
//        sl2.setText("Monsieur "+agee.getPrenom()+" suit une traitement \n de "+t.getDureeEnJourDeTraitement()+" jours, \n A noter: "+t.getTraitementDesc()+"\n");
        sl2.setText("Monsieur "
                        +agee.getPrenom()
                        +" a un Traitement médicamenteux à suivre.\n"
                        +" Vous avez lui ordonnoncé le médicament :\n "
                        +t.getNomMedicament()+", dont il doit le prendre pendant "
                        +t.getDureeEnJourDeTraitement()+jourS
                        +".\n A noter: "+t.getTraitementDesc()+".\n "
                        +AvantApres
                        +".");
        Font largeItalicSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_ITALIC, Font.SIZE_LARGE);
        sl2.getTextAllStyles().setFont(largeItalicSystemFont);
        sl2.getAllStyles().setPadding(60, 60, 60, 60);
        add(sl2);
        
        supp = new Button();
        supp.setText("Supprimer");
//        supp.setUIID("SUPPRIMER");
        supp.getStyle().setBgColor(0xFF4500);
        add(supp);
        supp.addActionListener(e->{
            TraitementMedicalService tmss = new TraitementMedicalService();
            tmss.supprimer1(t.getId());
            new DetailTraitement( res ,  agee ,  t).show();
        });
        
        ComboBox <String> combo ;
        ComboBox <String> combo1 ;
        ComboBox <String> combo2 ;
        ComboBox <String> combo3 ;
        
        
        combo = new ComboBox<> ();
        UserService us = new UserService();
        ArrayList<User> l = us.getAllUser();
        
        
        
        TraitementMedicalService tms = new TraitementMedicalService();
        listTraitementAgee = tms.getAllTraitementOfUser(agee);
        
        btnTab = new Button[listTraitementAgee.size()];
        sl = new SpanLabel[listTraitementAgee.size()];

        FontImage arrowDown = FontImage.createMaterial(FontImage.MATERIAL_KEYBOARD_ARROW_DOWN, "Label", 3);
        add(new Label("Modifier ce Traitement de L'Agée "+agee.getPrenom(), "TodayTitle"));
        int i = 0 ;
        
        
        
        Toolbar tb = getToolbar();
        tb.setTitleCentered(false);
        Image profilePic = res.getImage("user-picture.png");
        Image mask = res.getImage("round-mask.png");
        profilePic = profilePic.fill(mask.getWidth(), mask.getHeight());
        Label profilePicLabel = new Label(profilePic, "ProfilePicTitle");
        profilePicLabel.setMask(mask.createMask());

        Button menuButton = new Button("");
        Button back = new Button("");
        menuButton.setUIID("Title");
        menuButton.setUIID("back");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
        FontImage.setMaterialIcon(back, FontImage.MATERIAL_BACKSPACE);
        menuButton.addActionListener(e -> getToolbar().openSideMenu());
        back.addActionListener(e->{
            DetailTraitement dt = new DetailTraitement(res , agee , t);
            dt.show();
        });
//        this.getToolbar().addCommandToLeftBar("", res.getImage("low.png"), e->{
//            ProfileForm pf = new ProfileForm(res);
//            pf.show();
//        });
        
        Container remainingTasks = BoxLayout.encloseY(
                        new Label(l.size()+"", "CenterTitle"),
                        new Label("Nombre des Agées", "CenterSubTitle")
                );
        remainingTasks.setUIID("RemainingTasks");
        Container completedTasks = BoxLayout.encloseY(
                        new Label(""+listTraitementAgee.size(), "CenterTitle"),
                        new Label("Nombre des Traitements", "CenterSubTitle")
        );
        completedTasks.setUIID("CompletedTasks");
        completedTasks.getStyle().setBgColor(0x048B9A);


        Container titleCmp = BoxLayout.encloseY(
                        FlowLayout.encloseIn(menuButton),
                        FlowLayout.encloseIn(back),
                        BorderLayout.centerAbsolute(
                                BoxLayout.encloseY(
                                    new Label("Docteur Hassen", "Title"),
                                    new Label("Happy Olds", "SubTitle")
                                )
                            ).add(BorderLayout.WEST, profilePicLabel),
                        GridLayout.encloseIn(2, remainingTasks, completedTasks)
                );
        
        FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ADD);
        fab.getAllStyles().setMarginUnit(Style.UNIT_TYPE_PIXELS);
        fab.getAllStyles().setMargin(BOTTOM, completedTasks.getPreferredH() - fab.getPreferredH() / 2);
        tb.setTitleComponent(fab.bindFabToContainer(titleCmp, CENTER, BOTTOM));
                        
        
        TextField txtf1 = new TextField("", "Nom du Medicament", 20, TextField.ANY) ;
        TextField txtf2 = new TextField("", "Description", 20, TextField.EMAILADDR) ;
        TextField txtf3 = new TextField("", "Durée du traitement", 20, TextField.EMAILADDR) ;
        combo1 = new ComboBox<> ("avant","apres","non");
        combo2 = new ComboBox<> ("avant","apres","non");
        combo3 = new ComboBox<> ("avant","apres","non");
        petitDejeuner = new Label ("Petit Déjeuner");
        dejeuner = new Label ("Déjeuner");
        diner = new Label ("Diner");
        
        Button Enregistrer = new Button("Mise à jour");
        Enregistrer.setUIID("LoginButton");
        Enregistrer.addActionListener(e -> {
            TraitementMedical tm = new TraitementMedical();
            tm.setId(t.getId());
            tm.setIdAgee(t.getIdAgee());
System.out.println("_____________________________________combo index_______________________"+combo.getModel().getSelectedIndex());
            switch(combo.getModel().getSelectedIndex()){
                case 0 : {
                    System.out.println("_______________________________________________________________"+l.get(0).toString());
                    tm.setAgee(l.get(0));
                    break ;
                }
                case 1 :{
                    System.out.println("_______________________________________________________________"+l.get(1).toString());
                    tm.setAgee(l.get(1));
                    break ;
                }
                case 2 :{
                    tm.setAgee(l.get(2));
                    break ;
                }
                case 3 :{
                    tm.setAgee(l.get(3));
                    break ;
                }
                case 4 :{
                    tm.setAgee(l.get(4));
                    break ;
                }
                case 5 :{
                    tm.setAgee(l.get(5));
                    break ;
                }
            }
            
            switch(combo1.getModel().getSelectedIndex()){
                case 0 : {
                    tm.setPetitDejeuner("avant");
                    break ;
                }
                case 1 :{
                    tm.setPetitDejeuner("apres");   
                    break ;
                }
                case 2 :{
                    tm.setPetitDejeuner("non");   
                    break ;
                }
            }
            switch(combo2.getModel().getSelectedIndex()){
                case 0 : {
                    tm.setDejeuner("avant");
                    break ;
                }
                case 1 :{
                    tm.setDejeuner("apres");   
                    break ;
                }
                case 2 :{
                    tm.setDejeuner("non");   
                    break ;
                }
            }
            switch(combo3.getModel().getSelectedIndex()){
                case 0 : {
                    tm.setDiner("avant");
                    break ;
                }
                case 1 :{
                    tm.setDiner("apres");   
                    break ;
                }
                case 2 :{
                    tm.setDiner("non");   
                    break ;
                }
            }
            tm.setNomMedicament(txtf1.getText());
            tm.setTraitementDesc(txtf2.getText());
            tm.setDureeEnJourDeTraitement(parseInt(txtf3.getText()));
            TraitementMedicalService tms2 = new TraitementMedicalService();
            tms2.modifTraitement(tm , agee);
            new DetailTraitement(res , agee , t).show();
        });

         
//        txtf1.getAllStyles().setMargin(LEFT, 0);
        Label loginIcon = new Label("", "TextField");
        loginIcon.getAllStyles().setMargin(RIGHT, 0);
        FontImage.setMaterialIcon(loginIcon, FontImage.MATERIAL_PERSON_OUTLINE, 3);
        
        
        

        
        
//        combo.setRenderer(new GenericListCellRenderer<>(new MultiButton(), new MultiButton()));


        txtf1.setText(t.getNomMedicament());
        txtf2.setText(t.getTraitementDesc());
        txtf3.setText(""+t.getDureeEnJourDeTraitement());
        combo1.setSelectedItem(t.getPetitDejeuner());
        combo2.setSelectedItem(t.getDejeuner());
        combo3.setSelectedItem(t.getDiner());

        
        
        
        
        
        
        
        
        
        
//        add(txtf1);
//        add(txtf2);
//        add(txtf3);
//        add(petitDejeuner);
//        add(combo1);
//        add(dejeuner);
//        add(combo2);
//        add(diner);
//        add(combo3);
//        add(Enregistrer);
        
        
        
        this.getStyle().setBgColor(0xFFFFFF);

        
        Label addIcon = new Label("", "TextField");
        Label descIcon = new Label("", "TextField");
        Label dureeIcon = new Label("", "TextField");
        addIcon.getAllStyles().setMargin(RIGHT, 0);
        FontImage.setMaterialIcon(addIcon, FontImage.MATERIAL_ADD_BOX, 3);
        descIcon.getAllStyles().setMargin(RIGHT, 0);
        

        
        FontImage.setMaterialIcon(descIcon, FontImage.MATERIAL_DESCRIPTION, 3);
        FontImage.setMaterialIcon(dureeIcon, FontImage.MATERIAL_DATE_RANGE , 3);
        addIcon.getAllStyles().setBgColor(0x000000);
        addIcon.getAllStyles().setFgColor(0x000000);
//        addIcon.getAllStyles().setBgColor(0x000000);
        Button btnBlack = new Button();
//        btnBlack.set
        addIcon.setUIID(Enregistrer.getUIID());
        descIcon.setUIID(Enregistrer.getUIID());
        dureeIcon.setUIID(Enregistrer.getUIID());
            



            
        combo.getStyle().setBgTransparency(100);
        combo.getStyle().setMargin(30, BOTTOM, 500, 100);
        combo1.getStyle().setBgTransparency(100);
        combo1.getStyle().setMargin(30, BOTTOM, 500, 100);
        combo2.getStyle().setBgTransparency(100);
        combo2.getStyle().setMargin(30, BOTTOM, 500, 100);
        combo3.getStyle().setBgTransparency(100);
        combo3.getStyle().setMargin(30, BOTTOM, 500, 100);




//            txtf1.getStyle().setMargin(30, BOTTOM, 30, RIGHT);
        txtf1.getStyle().setFgColor(0x000000);
//            txtf2.getStyle().setMargin(30, BOTTOM, 30, RIGHT);
        txtf2.getStyle().setFgColor(0x000000);
//            txtf3.getStyle().setMargin(30, BOTTOM, 30, RIGHT);
        txtf3.getStyle().setFgColor(0x000000);
        petitDejeuner.getStyle().setMargin(30, BOTTOM, 30, RIGHT);
        petitDejeuner.getStyle().setFgColor(0x000000);
        dejeuner.getStyle().setMargin(30, BOTTOM, 30, RIGHT);
        dejeuner.getStyle().setFgColor(0x000000);
        diner.getStyle().setMargin(30, BOTTOM, 30, RIGHT);
        diner.getStyle().setFgColor(0x000000);





        Font largeBoldMonospaceFont = Font.createSystemFont(Font.FACE_MONOSPACE, Font.STYLE_BOLD, Font.SIZE_LARGE);
        txtf1.getStyle().setFgColor(0x24445C);
        txtf2.getStyle().setFgColor(0x24445C);
        txtf3.getStyle().setFgColor(0x24445C);
        txtf1.setGrowLimit(10000);
        txtf1.getStyle().setFont(largeBoldMonospaceFont);
        txtf2.getStyle().setFont(largeBoldMonospaceFont);
        txtf3.getStyle().setFont(largeBoldMonospaceFont);


        Container by = BoxLayout.encloseY(

            BorderLayout.center(txtf1).
                    add(BorderLayout.WEST, addIcon),
            BorderLayout.center(txtf2).
                    add(BorderLayout.WEST, descIcon),
            BorderLayout.center(txtf3).
                    add(BorderLayout.WEST, dureeIcon),
            BorderLayout.west(petitDejeuner).
                    add(BorderLayout.EAST, combo1),
            BorderLayout.west(dejeuner).
                    add(BorderLayout.EAST, combo2),
            BorderLayout.west(diner).
                    add(BorderLayout.EAST, combo3),

            Enregistrer
        );
        
        add(by);
        fab.setHidden(true);
        System.out.println("fab set hiden");
            

         
        
        
        
        
        actLst(res , agee);
        setupSideMenu(res);
    }
    
        
    
    public void actLst(Resources res , User agee){
        for(int i = 0 ; i<listTraitementAgee.size() ; i++){
//            ActionListenerDetailsMedicament alc = new ActionListenerDetailsMedicament(this ,btnTab[i] , sl[i] , listTraitementAgee.get(i) , agee , res);
            
        }
    }
    
    private Image createCircleLine(int color, int height, boolean first) {
        Image img = Image.createImage(height, height, 0);
        Graphics g = img.getGraphics();
        g.setAntiAliased(true);
        g.setColor(0xcccccc);
        int y = 0;
        if(first) {
            y = height / 6 + 1;
        }
        g.drawLine(height / 2, y, height / 2, height);
        g.drawLine(height / 2 - 1, y, height / 2 - 1, height);
        g.setColor(color);
        g.fillArc(height / 2 - height / 4, height / 6, height / 2, height / 2, 0, 360);
        return img;
    }

    @Override
    protected void showOtherForm(Resources res) {
        new StatsForm(res).show();
    }
//    public DetailTraitement getObjectDetail (){
//        return this ;
//    }
}

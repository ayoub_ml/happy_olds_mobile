/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codename1.uikit.materialscreens;


import com.codename1.components.FloatingActionButton;
import com.codename1.components.InteractionDialog;
import com.codename1.components.ToastBar;
import com.codename1.googlemaps.MapContainer;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Dialog;
import com.codename1.ui.Label;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.codename1.io.Log;
import com.codename1.maps.Coord;
import com.codename1.messaging.Message;
import com.codename1.notifications.LocalNotification;
import com.codename1.notifications.LocalNotificationCallback;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.SideMenuBar;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.geom.Rectangle;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import entity.MapLocation;
import entity.TraitementMedical;
import entity.User;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import service.MapLocationService;
import service.TraitementMedicalService;
import service.UserService;

/**
 *
 * @author autonome
 */
public class Map  extends SideMenuBaseForm {
        private static final String HTML_API_KEY = "AIzaSyBWeRU02YUYPdwRuMFyTKIXUbHjq6e35Gw";
//    private static final String HTML_API_KEY = "AIzaSyDDFM_cdybEBB98TYz5jWuvWZ0T7KtAj1M";
    
//        private Form current;

    User agee ;
    ArrayList<User> listAgee ;
    UserService us ;
    ComboBox comboAgees ;
    
    final MapContainer cnt;
    FontImage markerImg ;
            
    public Map(Resources res) {
        
        
        Form hi = new Form("Native Maps Test");
        hi.setLayout(new BorderLayout());
        cnt = new MapContainer(HTML_API_KEY);

        us = new UserService();
        listAgee = us.getAllUser();
        comboAgees = new ComboBox();
        comboAgees.getStyle().setOpacity(100);
        for (User u : listAgee){
            comboAgees.addItem(u.getPrenom());
        }
        
        Toolbar tb = hi.getToolbar();
        tb.setTitleCentered(false);
        
        Button menuButton = new Button("");
        menuButton.setUIID("Title");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
        menuButton.addActionListener(e -> getToolbar().openSideMenu());
        
        Container titleCmp = BoxLayout.encloseY(
                        FlowLayout.encloseIn(menuButton)
                        
                );
        FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ADD);
        fab.getAllStyles().setMarginUnit(Style.UNIT_TYPE_PIXELS);
        tb.setTitleComponent(titleCmp);
//        tb.setTitleComponent(fab.bindFabToContainer(titleCmp, CENTER, BOTTOM));
//        tb.addCommandToRightBar(e->this.show());
        
        Button btnMoveCamera = new Button("All Location");
        btnMoveCamera.addActionListener(e->{
//            cnt.setCameraPosition(new Coord(-33.867, 151.206));
            cnt.setCameraPosition(new Coord(35, 10));
            System.out.println("---------------------------------------------------------------------------");
            System.out.println("---------------------------------------------------------------------------");
            System.out.println("---------------------------------------------------------------------------");
            System.out.println("---------------------------------------------------------------------------");
            System.out.println("---------------------------------------------------------------------------");
            System.out.println("---------------------------------------------------------------------------");
            System.out.println("---------------------------------------------------------------------------");
            
            MapLocationService mls = new MapLocationService();
            ArrayList<MapLocation> listLocation ;
            listLocation = mls.getAllLocations();
            System.out.println(listLocation);
            for(MapLocation ml : listLocation){
                System.out.println(ml);
                String txt = "ageeeeee"; //agee.getPrenom()+"'s house";
                    cnt.addMarker(
                            EncodedImage.createFromImage(markerImg, false),
                            cnt.getCoordAtPosition(ml.getX(), ml.getY()),
                            "abc",
                            "",
                            e3->{
                                    ToastBar.showMessage("You clicked "+txt, FontImage.MATERIAL_PLACE);
                            }
                    );
            }
            System.out.println("---------------------------------------------------------------------------");
            System.out.println("---------------------------------------------------------------------------");
            System.out.println("---------------------------------------------------------------------------");
            System.out.println("---------------------------------------------------------------------------");
            System.out.println("---------------------------------------------------------------------------");
            System.out.println("---------------------------------------------------------------------------");
            System.out.println("---------------------------------------------------------------------------");
        });
        Style s = new Style();
        s.setFgColor(0xff0000);
        s.setBgTransparency(0);
        markerImg = FontImage.createMaterial(FontImage.MATERIAL_PLACE, s, Display.getInstance().convertToPixels((float)0.5));

        Button btnAddMarker = new Button("Add Marker");
        btnAddMarker.addActionListener(e->{

            cnt.setCameraPosition(new Coord(18, 5));
            cnt.addMarker(
                    EncodedImage.createFromImage(markerImg, false),
                    cnt.getCameraPosition(),
                    "Hi marker",
                    "Optional long description",
                     evt -> {
                             ToastBar.showMessage("You clicked the marker", FontImage.MATERIAL_PLACE);
                     }
            );

        });
        

        Button btnAddPath = new Button("Add Path");
        btnAddPath.addActionListener(e->{

            cnt.addPath(
                    cnt.getCameraPosition(),
                    new Coord(-33.866, 151.195), // Sydney
                    new Coord(-18.142, 178.431),  // Fiji
                    new Coord(21.291, -157.821),  // Hawaii
                    new Coord(37.423, -122.091)  // Mountain View
            );
        });

        Button btnClearAll = new Button("Clear All");
        btnClearAll.addActionListener(e->{
            cnt.clearMapLayers();
        });
        
        
        
        
        
             
        comboAgees.addActionListener(e -> {
            agee  = new User();
            System.out.println("_____________________________________combo index_______________________"+comboAgees.getModel().getSelectedIndex());
            switch(comboAgees.getModel().getSelectedIndex()){
                case 0 : {
                    System.out.println("_______________________________________________________________"+listAgee.get(0).toString());
                    agee = listAgee.get(0);
                    break ;
                }
                case 1 :{
                    System.out.println("_______________________________________________________________"+listAgee.get(1).toString());
                    agee = listAgee.get(1);
                    break ;
                }
                case 2 :{
                    agee = listAgee.get(2);
                    break ;
                }
                case 3 :{
                    agee = listAgee.get(3);
                    break ;
                }
                case 4 :{
                    agee = listAgee.get(4);
                    break ;
                }
                case 5 :{
                    agee = listAgee.get(5);
                    break ;
                }
                case 6 :{
                    agee = listAgee.get(6);
                    break ;
                }
                case 7 :{
                    agee = listAgee.get(7);
                    break ;
                }
            
            }
        });
      

        cnt.addTapListener(e->{
            MapLocation ml = new MapLocation();
            MapLocationService mls = new MapLocationService();
            ml.setX(e.getX());
            ml.setY(e.getY());
            ml.setAgee(agee);
            mls.addLocation(ml);
            TextField enterName = new TextField();
            Container wrapper = BoxLayout.encloseY(new Label("Name:"), enterName);
            InteractionDialog dlg = new InteractionDialog("Add Marker");
            dlg.getContentPane().add(wrapper);
            enterName.setDoneListener(e2->{
//                String txt = enterName.getText();
                String txt = agee.getPrenom()+"'s location";
                cnt.addMarker(
                        EncodedImage.createFromImage(markerImg, false),
                        cnt.getCoordAtPosition(e.getX(), e.getY()),
                        enterName.getText(),
                        "",
                        e3->{
                                ToastBar.showMessage("You clicked "+txt, FontImage.MATERIAL_PLACE);
                        }
                );
                dlg.dispose();
            });
            dlg.showPopupDialog(new Rectangle(e.getX(), e.getY(), 10, 10));
            enterName.startEditingAsync();
        });
        
//        cnt.addTapListener(e->{
//            MapLocation ml = new MapLocation();
//            MapLocationService mls = new MapLocationService();
//            
//            TextField enterName = new TextField();
//            Container wrapper = BoxLayout.encloseY(new Label("Name:"), enterName);
//            InteractionDialog dlg = new InteractionDialog("Add Marker");
//            dlg.getContentPane().add(wrapper);
//            enterName.setDoneListener(e2->{
////                String txt = enterName.getText();
//                String txt = agee.getPrenom()+"'s house";
//                cnt.addMarker(
//                        EncodedImage.createFromImage(markerImg, false),
//                        cnt.getCoordAtPosition(e.getX(), e.getY()),
//                        enterName.getText(),
//                        "",
//                        e3->{
//                                ToastBar.showMessage("You clicked "+txt, FontImage.MATERIAL_PLACE);
//                        }
//                );
//                dlg.dispose();
//            });
//            dlg.showPopupDialog(new Rectangle(e.getX(), e.getY(), 10, 10));
//            enterName.startEditingAsync();
//        });

        Container root = LayeredLayout.encloseIn(
                BorderLayout.center(cnt),
                BorderLayout.south(
                        FlowLayout.encloseBottom( btnMoveCamera, btnAddMarker, btnAddPath, btnClearAll)
                )
//                BorderLayout.north(
//                        FlowLayout.encloseBottom(comboAgees)
//                )
        );
        
        

        Container comboContainer = new Container();
        comboContainer.setLayout(BoxLayout.x());
        comboContainer.add(comboAgees);
        
        Button btnGo = new Button("Go");
        btnGo.addActionListener(e->{
            System.out.println("agee to string btn go action : --- "+agee.toString());
            cnt.clearMapLayers();
            MapLocationService mls = new MapLocationService();
            ArrayList<MapLocation> listLocation ;
            listLocation = mls.getAllLocationsOfUser(agee);
            System.out.println(listLocation);
            for(MapLocation ml : listLocation){
                System.out.println(ml);
                String txt = agee.getPrenom()+"'s location"; //agee.getPrenom()+"'s house";
                    cnt.addMarker(
                            EncodedImage.createFromImage(markerImg, false),
                            cnt.getCoordAtPosition(ml.getX(), ml.getY()),
                            "here "+agee.getPrenom()+" Location",
                            "",
                            e3->{
                                    ToastBar.showMessage("You clicked "+txt, FontImage.MATERIAL_PLACE);
                            }
                    );
            }
        });
        
        comboContainer.add(btnGo);
        
        hi.getStyle().setBgColor(0xADD8E6);
        hi.add(BorderLayout.SOUTH, comboContainer);
        hi.add(BorderLayout.CENTER, root);
        hi.show();
        setupSideMenu(res);
    }

    @Override
    protected void showOtherForm(Resources res) {
        new StatsForm(res).show();
    }

    private void addMarkerFromDB() {
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("---------------------------------------------------------------------------");

        MapLocationService mls = new MapLocationService();
        ArrayList<MapLocation> listLocation ;
        listLocation = mls.getAllLocations();
        System.out.println(listLocation);
        for(MapLocation ml : listLocation){
            System.out.println(ml);
            String txt = agee.getPrenom()+"'s location";
                cnt.addMarker(
                        EncodedImage.createFromImage(markerImg, false),
                        cnt.getCoordAtPosition((int)ml.getX(),(int) ml.getY()),
                        "here "+agee.getPrenom()+" Location",
                        "",
                        e3->{
                                ToastBar.showMessage("You clicked "+txt, FontImage.MATERIAL_PLACE);
                        }
                );
        }
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("---------------------------------------------------------------------------");
    }
        
        
    
        


    
    
}

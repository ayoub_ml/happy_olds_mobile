/*
 * Copyright (c) 2016, Codename One
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

package com.codename1.uikit.materialscreens;

import com.codename1.components.FloatingActionButton;
import com.codename1.components.MultiButton;
import com.codename1.notifications.LocalNotification;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.list.GenericListCellRenderer;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import entity.TraitementMedical;
import entity.User;
import static java.lang.Integer.parseInt;
import java.util.Map;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.Alert;
import service.TraitementMedicalService;
import service.UserService;

/**
 * Represents a user profile in the app, the first form we open after the walkthru
 *
 * @author Shai Almog
 */
public class ProfileForm extends SideMenuBaseForm {
    
    LocalNotification n;
  
    Container ajoutCont ;
            
    Label ageeLabel ;
    Label petitDejeunerLabel ;
    Label dejeunerLabel ;
    Label dinerLabel ;
    
    ComboBox <String> combo ;    
    ComboBox <String> combo1 ;
    ComboBox <String> combo2 ;
    ComboBox <String> combo3 ;
    
    TextField txtf1  ;
    TextField txtf2  ;
    TextField txtf3  ;
    
    Button btnDetail ;
    
    Button [] btnTab ;
    int [] intBtnTab ;
    ArrayList<User> l ;
    ArrayList<TraitementMedical> listdesTraitmentsTotal ;
    
    int [] tabColeur = {0xd997f1,0x5ae29d,0x4dc2ff} ;

    public ProfileForm(Resources res) {
        
        super(BoxLayout.y());
        
        this.setLayout(BoxLayout.y());
        
        
        
        btnDetail  = new Button();
        
        
        
        
        combo = new ComboBox<> ();
        UserService us = new UserService();
        TraitementMedicalService tmService = new TraitementMedicalService();
        l = us.getAllUser();
        listdesTraitmentsTotal = tmService.getAllTraitement();
        
        btnTab = new Button[l.size()];

        
        
        
        
        Toolbar tb = getToolbar();
        tb.setTitleCentered(false);
        Image profilePic = res.getImage("user-picture.png");
        Image mask = res.getImage("round-mask.png");
        profilePic = profilePic.fill(mask.getWidth(), mask.getHeight());
        Label profilePicLabel = new Label(profilePic, "ProfilePicTitle");
        profilePicLabel.setMask(mask.createMask());

        Button menuButton = new Button("");
        menuButton.setUIID("Title");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
        menuButton.addActionListener(e -> getToolbar().openSideMenu());
        
        Container remainingTasks = BoxLayout.encloseY(
                        new Label(l.size()+"", "CenterTitle"),
                        new Label("Nombre des Agées", "CenterSubTitle")
                );
        remainingTasks.setUIID("RemainingTasks");
        Container completedTasks = BoxLayout.encloseY(
                        new Label(listdesTraitmentsTotal.size()+"", "CenterTitle"),
                        new Label("Nombre Total des Traitements", "CenterSubTitle")
        );
        completedTasks.setUIID("CompletedTasks");
        completedTasks.getStyle().setBgColor(0x048B9A);

        Container titleCmp = BoxLayout.encloseY(
                        FlowLayout.encloseIn(menuButton),
                        BorderLayout.centerAbsolute(
                                BoxLayout.encloseY(
                                    new Label("  Docteur Hassen", "Title"),
                                    new Label("UI/UX Designer", "SubTitle")
                                )
                            ).add(BorderLayout.WEST, profilePicLabel),
                        GridLayout.encloseIn(2, remainingTasks, completedTasks)
                );
        
        FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ADD);
        fab.getAllStyles().setMarginUnit(Style.UNIT_TYPE_PIXELS);
        fab.getAllStyles().setMargin(BOTTOM, completedTasks.getPreferredH() - fab.getPreferredH() / 2);
        tb.setTitleComponent(fab.bindFabToContainer(titleCmp, CENTER, BOTTOM));
                        
        add(new Label("Liste des âgées", "TodayTitle"));
        
        txtf1 = new TextField("", "Nom du Medicament", 20, TextField.EMAILADDR) ;
        txtf1 = new TextField("", "Nom du Medicament", 20, TextField.EMAILADDR) ;
        txtf2 = new TextField("", "Description", 20, TextField.EMAILADDR) ;
        txtf3 = new TextField("", "Durée du traitement", 20, TextField.EMAILADDR) ;
        combo1 = new ComboBox<> ("avant","apres","non");
        combo2 = new ComboBox<> ("avant","apres","non");
        combo3 = new ComboBox<> ("avant","apres","non");
        ageeLabel = new Label ("Agée :");
        petitDejeunerLabel = new Label ("Petit Déjeuner :");
        dejeunerLabel = new Label ("Déjeuner :");
        dinerLabel = new Label ("Diner :");
        
        Button Enregistrer = new Button("Enregistrer");
        Enregistrer.setUIID("LoginButton");
        
        n = new LocalNotification();
        n.setId("demo-notification");
        n.setAlertBody("It's time to take a break and look at me");
        n.setAlertTitle("Break Time!");
        n.setAlertSound("/notification_sound_bells.mp3"); //file name must begin with notification_sound


        Display.getInstance().scheduleLocalNotification(
                n,
                System.currentTimeMillis() + 10 * 1000, // fire date/time
                LocalNotification.REPEAT_MINUTE  // Whether to repeat and what frequency
        );
        
        Enregistrer.addActionListener(e -> {
            TraitementMedical tm = new TraitementMedical();
System.out.println("_____________________________________combo index_______________________"+combo.getModel().getSelectedIndex());
            switch(combo.getModel().getSelectedIndex()){
                case 0 : {
                    System.out.println("_______________________________________________________________"+l.get(0).toString());
                    tm.setAgee(l.get(0));
                    break ;
                }
                case 1 :{
                    System.out.println("_______________________________________________________________"+l.get(1).toString());
                    tm.setAgee(l.get(1));
                    break ;
                }
                case 2 :{
                    tm.setAgee(l.get(2));
                    break ;
                }
                case 3 :{
                    tm.setAgee(l.get(3));
                    break ;
                }
                case 4 :{
                    tm.setAgee(l.get(4));
                    break ;
                }
                case 5 :{
                    tm.setAgee(l.get(5));
                    break ;
                }
            
            }
            
            switch(combo1.getModel().getSelectedIndex()){
                case 0 : {
                    tm.setDejeuner("avant");
                    break ;
                }
                case 1 :{
                    tm.setDejeuner("apres");   
                    break ;
                }
                case 2 :{
                    tm.setDejeuner("non");   
                    break ;
                }
            }
            switch(combo2.getModel().getSelectedIndex()){
                case 0 : {
                    tm.setDiner("avant");
                    break ;
                }
                case 1 :{
                    tm.setDiner("apres");   
                    break ;
                }
                case 2 :{
                    tm.setDiner("non");   
                    break ;
                }
            }
            switch(combo3.getModel().getSelectedIndex()){
                case 0 : {
                    tm.setPetitDejeuner("avant");
                    break ;
                }
                case 1 :{
                    tm.setPetitDejeuner("apres");   
                    break ;
                }
                case 2 :{
                    tm.setPetitDejeuner("non");   
                    break ;
                }
            }
            if(txtf1.getText().length()<3 || txtf2.getText().length()<3 ){
                Dialog.show("Humm", "Vérifiez vos informations...", "J'ai compris", null);
            }else if(estUnEntier(txtf3.getText())) {
                Dialog.show("Humm", "La durée est un entier ! écrivez bien...", "J'ai compris", null);                           
            }else{
                tm.setNomMedicament(txtf1.getText());
                tm.setTraitementDesc(txtf2.getText());
                tm.setDureeEnJourDeTraitement(parseInt(txtf3.getText()));
                TraitementMedicalService tms = new TraitementMedicalService();
                tms.ajoutTraitement(tm);
                new ProfileForm(res).show();
                
            }
            
        });

         
//        txtf1.getAllStyles().setMargin(LEFT, 0);
        Label addIcon = new Label("", "TextField");
        Label descIcon = new Label("", "TextField");
        Label dureeIcon = new Label("", "TextField");
        addIcon.getAllStyles().setMargin(RIGHT, 0);
        FontImage.setMaterialIcon(addIcon, FontImage.MATERIAL_ADD_BOX, 3);
        descIcon.getAllStyles().setMargin(RIGHT, 0);
        

        
        FontImage.setMaterialIcon(descIcon, FontImage.MATERIAL_DESCRIPTION, 3);
        FontImage.setMaterialIcon(dureeIcon, FontImage.MATERIAL_DATE_RANGE , 3);
        addIcon.getAllStyles().setBgColor(0x000000);
        addIcon.getAllStyles().setFgColor(0x000000);
//        addIcon.getAllStyles().setBgColor(0x000000);
        Button btnBlack = new Button();
//        btnBlack.set
        addIcon.setUIID(Enregistrer.getUIID());
        descIcon.setUIID(Enregistrer.getUIID());
        dureeIcon.setUIID(Enregistrer.getUIID());
        
        
        FontImage arrowDown = FontImage.createMaterial(FontImage.MATERIAL_KEYBOARD_ARROW_DOWN, "Label", 3);

        
        System.out.println("_____________hayla____________"+l.get(0));
        System.out.println("_____________hayla____________"+l.get(1));
        System.out.println("_____________hayla____________"+l.get(2));
        System.out.println("_____________hayla____________"+l.get(3));
        
        
        int i = 0 ;
        for (User u : l){
            
            combo.addItem(u.getPrenom());
            Container c = new Container();
            Form f = new Form();
            addButtonBottom(arrowDown, u.getPrenom(), tabColeur[i], true  , res);
            i++;
            i %= 3 ;
        }
        
//        addActionButton(res);
        actLst(res);
        

        

        FloatingActionButton fab2 = FloatingActionButton.createFAB(FontImage.MATERIAL_KEYBOARD_ARROW_UP);

        fab.addActionListener(e-> {

            this.getStyle().setBgColor(0xFFFFFF);

            
            ajoutCont = new Container();
            ajoutCont.setLayout(BoxLayout.y());


            
            combo.getStyle().setBgTransparency(100);
            combo.getStyle().setMargin(30, BOTTOM, 500, 100);
            combo.pointerHover(intBtnTab, intBtnTab);
            combo1.getStyle().setBgTransparency(100);
            combo1.getStyle().setMargin(30, BOTTOM, 500, 100);
            combo2.getStyle().setBgTransparency(100);
            combo2.getStyle().setMargin(30, BOTTOM, 500, 100);
            combo3.getStyle().setBgTransparency(100);
            combo3.getStyle().setMargin(30, BOTTOM, 500, 100);
            
            
            
            ageeLabel.getStyle().setMargin(30, BOTTOM, 30, RIGHT);
            ageeLabel.getAllStyles().setFgColor(0x000000);
//            txtf1.getStyle().setMargin(30, BOTTOM, 30, RIGHT);
            txtf1.getStyle().setFgColor(0x000000);
//            txtf2.getStyle().setMargin(30, BOTTOM, 30, RIGHT);
            txtf2.getStyle().setFgColor(0x000000);
//            txtf3.getStyle().setMargin(30, BOTTOM, 30, RIGHT);
            txtf3.getStyle().setFgColor(0x000000);
            petitDejeunerLabel.getStyle().setMargin(30, BOTTOM, 30, RIGHT);
            petitDejeunerLabel.getStyle().setFgColor(0x000000);
            dejeunerLabel.getStyle().setMargin(30, BOTTOM, 30, RIGHT);
            dejeunerLabel.getStyle().setFgColor(0x000000);
            dinerLabel.getStyle().setMargin(30, BOTTOM, 30, RIGHT);
            dinerLabel.getStyle().setFgColor(0x000000);
            
            
            
        
            
            Font largeBoldMonospaceFont = Font.createSystemFont(Font.FACE_MONOSPACE, Font.STYLE_BOLD, Font.SIZE_LARGE);
            txtf1.getStyle().setFgColor(0x24445C);
            txtf2.getStyle().setFgColor(0x24445C);
            txtf3.getStyle().setFgColor(0x24445C);
            txtf1.setGrowLimit(10000);
            txtf1.getStyle().setFont(largeBoldMonospaceFont);
            txtf2.getStyle().setFont(largeBoldMonospaceFont);
            txtf3.getStyle().setFont(largeBoldMonospaceFont);

            ajoutCont.add(fab2);
           
            Container by = BoxLayout.encloseY(
                
                BorderLayout.center(txtf1).
                        add(BorderLayout.WEST, addIcon),
                BorderLayout.center(txtf2).
                        add(BorderLayout.WEST, descIcon),
                BorderLayout.center(txtf3).
                        add(BorderLayout.WEST, dureeIcon),
                BorderLayout.west(ageeLabel).
                        add(BorderLayout.EAST, combo),
                BorderLayout.west(petitDejeunerLabel).
                        add(BorderLayout.EAST, combo1),
                BorderLayout.west(dejeunerLabel).
                        add(BorderLayout.EAST, combo2),
                BorderLayout.west(dinerLabel).
                        add(BorderLayout.EAST, combo3),
                
                Enregistrer
                
            );

            add(by);
            add(ajoutCont);
            ajoutCont.setHidden(false);
            fab.setHidden(true);
            System.out.println("fab set hiden");
            
            this.show();
        });
        
        ProfileForm pf = this ;
        fab2.addActionListener(e->{
            ajoutCont.setHidden(true);
            new ProfileForm(res).show();
        });
        
        
      
        

        setupSideMenu(res);
    }
    
    
    
    int idBtn = 0;
    
    private void addButtonBottom(Image arrowDown, String text, int color, boolean first ,Resources res) {
        btnTab[idBtn] = new Button ();
        MultiButton finishLandingPage = new MultiButton(text);
        finishLandingPage.setEmblem(arrowDown);
        finishLandingPage.setUIID("Container"+idBtn);
        finishLandingPage.setUIIDLine1("TodayEntry"+idBtn);
        finishLandingPage.setIcon(createCircleLine(color, finishLandingPage.getPreferredH(),  first));
        finishLandingPage.setIconUIID("Container"+idBtn);
        finishLandingPage.setLeadComponent(btnTab[idBtn]);
        add(FlowLayout.encloseIn(finishLandingPage));       
        idBtn++;       
    }
    
    public void actLst(Resources res){
        for(int i = 0 ; i<l.size() ; i++){
            ActionListnerClass alc = new ActionListnerClass(btnTab[i] , l.get(i) , res);
            
        }
    }
    

    private Image createCircleLine(int color, int height, boolean first) {
        Image img = Image.createImage(height, height, 0);
        Graphics g = img.getGraphics();
        g.setAntiAliased(true);
        g.setColor(0xcccccc);
        int y = 0;
        if(first) {
            y = height / 6 + 1;
        }
        g.drawLine(height / 2, y, height / 2, height);
        g.drawLine(height / 2 - 1, y, height / 2 - 1, height);
        g.setColor(color);
        g.fillArc(height / 2 - height / 4, height / 6, height / 2, height / 2, 0, 360);
        return img;
    }
    
    public boolean estUnEntier(String chaine) {
		try {
			Integer.parseInt(chaine);
		} catch (NumberFormatException e){
			return false;
		}
 
		return true;
	}
    
    

    @Override
    protected void showOtherForm(Resources res) {
        new StatsForm(res).show();
    }
}

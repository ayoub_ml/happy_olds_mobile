/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author autonome
 */
public class MapLocation {
    
    int idAgee ;
    User agee ;
    int x , y ;

    public MapLocation() {
    }

    public MapLocation(int idAgee, User agee, int x, int y) {
        this.idAgee = idAgee;
        this.agee = agee;
        this.x = x;
        this.y = y;
    }

    public int getIdAgee() {
        return idAgee;
    }

    public void setIdAgee(int idAgee) {
        this.idAgee = idAgee;
    }

    public User getAgee() {
        return agee;
    }

    public void setAgee(User agee) {
        this.agee = agee;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "to String location __________ x: "+x
                +"  y: "+y
                +"   id :"+idAgee;
    }
    
    
    
    
    
    
}

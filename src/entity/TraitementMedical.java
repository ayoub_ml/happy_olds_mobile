/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author autonome
 */
public class TraitementMedical {
    

    private int id ;
    private int dureeEnJourDeTraitement ;
    private int idAgee;
    private String nomMedicament;
    private String traitementDesc;
    private String dejeuner;
    private String petitDejeuner;
    private String diner;
    
    private User agee ;

    public TraitementMedical() {
    }

    public TraitementMedical(int id, int dureeEnJourDeTraitement, int idAgee, String nomMedicament, String traitementDesc, String dejeuner, String petitDejeuner, String diner) {
        this.id = id;
        this.dureeEnJourDeTraitement = dureeEnJourDeTraitement;
        this.idAgee = idAgee;
        this.nomMedicament = nomMedicament;
        this.traitementDesc = traitementDesc;
        this.dejeuner = dejeuner;
        this.petitDejeuner = petitDejeuner;
        this.diner = diner;
    }

    public TraitementMedical(int id, int dureeEnJourDeTraitement, String nomMedicament, String traitementDesc, String dejeuner, String petitDejeuner, String diner) {
        this.id = id;
        this.dureeEnJourDeTraitement = dureeEnJourDeTraitement;
        this.idAgee = idAgee;
        this.nomMedicament = nomMedicament;
        this.traitementDesc = traitementDesc;
        this.dejeuner = dejeuner;
        this.petitDejeuner = petitDejeuner;
        this.diner = diner;    
    }

    
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDureeEnJourDeTraitement() {
        return dureeEnJourDeTraitement;
    }

    public void setDureeEnJourDeTraitement(int dureeEnJourDeTraitement) {
        this.dureeEnJourDeTraitement = dureeEnJourDeTraitement;
    }

    public int getIdAgee() {
        return idAgee;
    }

    public void setIdAgee(int idAgee) {
        this.idAgee = idAgee;
    }

    public String getNomMedicament() {
        return nomMedicament;
    }

    public void setNomMedicament(String nomMedicament) {
        this.nomMedicament = nomMedicament;
    }

    public String getTraitementDesc() {
        return traitementDesc;
    }

    public void setTraitementDesc(String traitementDesc) {
        this.traitementDesc = traitementDesc;
    }

    public String getDejeuner() {
        return dejeuner;
    }

    public void setDejeuner(String dejeuner) {
        this.dejeuner = dejeuner;
    }

    public String getPetitDejeuner() {
        return petitDejeuner;
    }

    public void setPetitDejeuner(String petitDejeuner) {
        this.petitDejeuner = petitDejeuner;
    }

    public String getDiner() {
        return diner;
    }

    public void setDiner(String diner) {
        this.diner = diner;
    }

    public User getAgee() {
        return agee;
    }

    public void setAgee(User agee) {
        this.agee = agee;
    }
    
    
    
    public String toString(){
        return"____toString Traitement____ id : "+id
                +"  Duree: "+dureeEnJourDeTraitement
                +"  idAgee: "+idAgee
                +"  nomMedicament: "+nomMedicament
                +"  traitementDesc: "+traitementDesc
                +"  dejeuner: "+dejeuner
                +"  petitDejeuner: "+petitDejeuner
                +"  diner: "+diner;
//                +"  ageeToStringgg: "+agee.toString();
    }
    

    
    
}

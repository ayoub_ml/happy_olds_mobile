/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author autonome
 */
public class User {
    
    private int id ; 
    private int cin ; 
    private String nom ;
    private String prenom ;

    public User() {
    }
    
    public User(int id , int cin , String nom , String prenom) {
        this.id = id ;
        this.cin = cin ;
        this.nom = nom ;
        this.prenom = prenom ;
    }
    
    public User(String nom) {
        this.nom = nom ;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCin() {
        return cin;
    }

    public void setCin(int cin) {
        this.cin = cin;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
    public String toString(){
        return "nom: "+nom
                +"   prenom: "+prenom
                +"   cin: "+cin
                +"   id: "+id;
    }
    
}
